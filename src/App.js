import AppRouter from './routers/AppRouter';
import './scss/index.scss';

function App() {
  return (
    <div className="App">
      <AppRouter/>
    </div>
  );
}

export default App;
