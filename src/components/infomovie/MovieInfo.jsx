import React, { useEffect, useState } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import moment from 'moment';
import Modal from '../movies/Modal';
import { API_KEY, API_URL } from '../../constants/constants'
import Loader from '../header/Loader';
import no_image from '../../images/image.jpg';

const MovieInfo = ({
  info: {
    id,
    title,
    poster_path,
    backdrop_path,
    release_date,
    genres,
    overview,
    tagline
  }
}) => {

  const [showModal, setShowModal] = useState(false);
  const [videoYt, setvideoYt] = useState();
  const [loading, setLoading] = useState(true);

  const getVideoYt = async (id) => {
    const res = await fetch(`${API_URL}/movie/${id}/videos?api_key=${API_KEY}&language=en-US`);
    const video = await res.json();
    setvideoYt(video);
    setLoading(false);
  }

  useEffect(() => {
    getVideoYt(id);
    setLoading(true)
  }, [id])

  let b_path = `https://image.tmdb.org/t/p/original${backdrop_path}`
  let p_path = `https://image.tmdb.org/t/p/original${poster_path}`

  if (loading === true) {
    return (
      <Loader />
    )
  }

  const trailer = videoYt.results;

  return (
    <section className="movie-info">
      <div className="movie-info__bg">
        <div
          className="movie-info__img"
          style={ backdrop_path ? { backgroundImage: `url('${b_path}')` } : { backgroundImage: `url('${no_image}')` } }
        >
          <div className="movie-info__bgblack" />
        </div>
        <div className="movie-info__details">
          <div className="img-poster">
            <LazyLoadImage
              src={ poster_path ? p_path : no_image }
              className="movie-info__poster"
            />
          </div>
          <div className="movie-info__description">
            <h2 className="movie-info__title">
              {title}
              <span style={{ fontStyle: 'italic' }} > ({moment(release_date, "YYYY-MM-DD").format("YYYY")})</span>
            </h2>
            <p>
              {
                genres.map(g => (
                  <span style={{ margin: '0px 5px' }} key={g.id} > &bull; {g.name}  </span>
                ))
              }
            </p>
            <div style={{ margin: '0' }}>
              <span style={{ margin: '10px 0px' }} > {tagline} </span>
              <h2>Resumen</h2>
              {
                overview.length === 0 ?
                  <p>------------</p>
                  :
                  <p className="movie-info__overview"> {overview} </p>
              }
              {
                trailer.length === 0 ?
                  null
                  :
                  (
                    <button
                      className="movie-info__trailerbtn"
                      onClick={() => setShowModal(!showModal)}
                    >
                      Ver Trailer
                    </button>
                  )
              }
            </div>
          </div>
        </div>
        {
          showModal === true && trailer.length > 0 ?
            <Modal setShowModal={setShowModal} trailer={trailer} />
            :
            null
        }
      </div>
    </section>
  )
}

export default MovieInfo
