import React from 'react'

const NextPage = ({ page, setPage }) => {
  return (
    <div className="next-page">
      <button disabled={ page === 1 ? `{true}` : null } onClick={() => setPage(page - 1)} >BACK</button>
      <button onClick={() => setPage(page + 1)} >NEXT</button>
    </div>
  )
}

export default NextPage
