import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const Navigation = ({page, setPage}) => {

  const [showMenu, setShowMenu] = useState(false);

  const fnHome = () => {
    setShowMenu(false);
    setPage(1)
  }

  return (
    <nav className="navigation-nav">
      <i className="fas fa-bars icono-movil" onClick={() => setShowMenu(!showMenu)} ></i>
      <ul className={` ${showMenu ? 'showMenu' : null} navigation-nav__ul`}>
        <li className="navigation-nav__li">
          <Link onClick={()=>fnHome()} to='/' >HOME</Link>
        </li>
        <li className="navigation-nav__li">
          <Link onClick={()=>fnHome()} to='/top_rated' >TOP RATED</Link>
        </li>
        <li className="navigation-nav__li">
          <Link onClick={()=>fnHome()} to='/upcoming' >UPCOMING</Link>
        </li>
      </ul>
    </nav>
  )
}

export default Navigation
