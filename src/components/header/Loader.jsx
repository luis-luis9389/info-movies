import React from 'react';
import loading from '../../images/loading.png';

const Loader = () => {
  return (
    <div className="spinner">
      <img src={loading} alt="loading spinner"/>
    </div>
  )
}

export default Loader