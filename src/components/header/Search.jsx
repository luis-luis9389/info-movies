import React, { useState } from 'react'
import { useHistory } from "react-router-dom";

const Search = () => {

  let history = useHistory();

  const [value, setValue] = useState({
    name: ''
  });

  const [error, setError] = useState(false);

  const { name } = value;

  const handleChange = (e) => {
    setValue({
      ...value,
      [e.target.name]: e.target.value
    })
  }


  const search = (id) => {
    history.push(`/results/${id}`)
  }

  const onSubmit = (e) => {
    e.preventDefault();
    if (name.trim() === '') {
      setError(true);
      setTimeout(() => {
      setError(false);
      }, 2000);
      return
    }
    search(name);
    setValue({
      name:''
    })
  }

  return (
    <>
      <form onSubmit={onSubmit} className="form-search">
        <input
          className="form-search__text"
          type="text"
          name="name"
          placeholder="Buscar..."
          onChange={handleChange}
          value={name}
          autoComplete='off'
        />
        <input
          className="form-search__submit"
          type="submit"
          value="Buscar"
        />
      </form>
      <Msj error={error} />
    </>
  )
}

const Msj = ({ error }) => {
  return (
    <>
      {
        error ? <span className="txt-warning">Empty Field</span> : null
      }
    </>
  )
}

export default Search
