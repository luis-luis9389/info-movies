import React from 'react';
import Navigation from './Navigation';
import Search from './Search';

const Header = ({ page, setPage }) => {
  return (
    <section className="header-container">
      <Navigation page={page} setPage={setPage} />
      <p>
        <strong>Bienvenidos a PeliculitasApp.</strong>
        <br />
        Millones de películas y series por descubrir. Explora ahora.
      </p>
      <Search />
    </section>
  )
}

export default Header
