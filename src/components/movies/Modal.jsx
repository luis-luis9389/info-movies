import React from 'react'

const Modal = ({ id, setShowModal, trailer }) => {
  let key = trailer[0].key;
  return (
    <section className="modal-video">
      <div className="modal-video__content">
        <div className="modal-video__close">
          <button
            onClick={() => setShowModal(false)}
          >
            X
        </button>
        </div>
        <iframe
          className="modal-video__frame"
          src={`https://www.youtube.com/embed/${key}`}
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer;
                autoplay;
                clipboard-write;
                encrypted-media;
                gyroscope;
                picture-in-picture"
          allowFullScreen>
        </iframe>
      </div>
    </section>
  )
}

export default Modal
