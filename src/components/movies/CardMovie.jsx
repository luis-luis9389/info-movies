import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { Link } from 'react-router-dom';
import image from '../../images/image.jpg';

const CardMovie = ({
  movie: { title,
    id,
    poster_path
  }
}) => {

  let poster = `https://image.tmdb.org/t/p/original${poster_path}`

  return (
    <div className="card-movie">
      <Link
        style={{
          textDecoration: 'none',
          textAlign: 'center',
          color: 'black'
        }}
        to={`/info/${id}`}
      >
        <LazyLoadImage
          src={poster_path ? poster : image}
          alt={title}
          width="150" />
        <span>{title}</span>
      </Link>
    </div>
  )
}

export default CardMovie
