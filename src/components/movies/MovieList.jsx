import React from 'react'
import CardMovie from './CardMovie'

const MovieList = ({movies}) => {

  return (
    <div className="container">
      <div className="movie-list">
        {
          movies.results.length === 0 ?
          <p style={{ fontSize:'2rem' }} >Not Found. </p>
          :
          movies.results.map((movie, i) => (
            <CardMovie key={i} movie={movie} />
          ))
        }
      </div>
    </div>
  )
}

export default MovieList
