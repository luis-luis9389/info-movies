import React, { useEffect, useState } from 'react'
import Loader from '../components/header/Loader';
import MovieList from '../components/movies/MovieList';
import { API_KEY, API_URL } from '../constants/constants'

const TopRated = ({ page, setPage }) => {

  const [topRated, setTopRated] = useState();
  const [loading, setLoading] = useState(true);

  const api_url = `${API_URL}/movie/top_rated?api_key=${API_KEY}&language=es-MX&page=${page}`;
  const getTopRated = async () => {
    const res = await fetch(api_url);
    const data = await res.json();
    setTopRated(data);
    setLoading(false);
  };

  useEffect(() => {
    setPage(1)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    getTopRated();
    setLoading(true);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page])

  if (loading === true) {
    return (
      <Loader />
    )
  }

  return (
    <section>
      <p className="title-category">Top Rated</p>
      {
        topRated.success === false ?
          <p style={{ textAlign: 'center', height: '400px' }} >{topRated.status_message}</p>
          :
          <MovieList movies={topRated} />
      }
    </section>
  )
}

export default TopRated
