import React, { useEffect, useState } from 'react'
import Loader from '../components/header/Loader';
import MovieList from '../components/movies/MovieList';
import { API_URL, API_KEY } from '../constants/constants';

const HomePage = ({ page, setPage }) => {

  const [populars, setPopulars] = useState();
  const [loading, setLoading] = useState(true);

  const api_url = `${API_URL}/movie/popular?api_key=${API_KEY}&language=es-MX&page=${page}`;
  const getPopular = async () => {
    const res = await fetch(api_url);
    const data = await res.json();
    setPopulars(data);
    setLoading(false);
  };

  useEffect(() => {
    setPage(1)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    getPopular();
    setLoading(true);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page])

  if (loading === true) {
    return (
      <Loader />
    )
  }

  return (
    <section style={{ margin: '50px 0' }}>
      <p className="title-category">
        Populars
      </p>
      {
        populars.success === false ?
          <p style={{ textAlign: 'center', height: '400px' }} >{populars.status_message}</p>
          :
          <MovieList movies={populars} />
      }
    </section>
  )
}

export default HomePage
