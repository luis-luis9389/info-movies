import React, { useEffect, useState } from 'react'
import Loader from '../components/header/Loader';
import MovieList from '../components/movies/MovieList';
import { API_URL, API_KEY } from '../constants/constants';

const Trending = ({ page, setPage }) => {

  const [upcoming, setUpcoming] = useState();
  const [loading, setLoading] = useState(true);

  const api_url = `${API_URL}/movie/upcoming?api_key=${API_KEY}&language=es-MX&page=${page}`;
  const getUpcoming = async () => {
    const res = await fetch(api_url);
    const data = await res.json();
    setUpcoming(data);
    setLoading(false)
  };

  useEffect(() => {
    setPage(1)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    getUpcoming();
    setLoading(true);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page])

  if (loading === true) {
    return (
      <Loader />
    )
  }

  return (
    <section>
      <p className="title-category">Upcoming</p>
      {
        upcoming.success === false ?
        <p style={{ textAlign:'center', height:'400px' }} >{upcoming.status_message}</p>
        :
        <MovieList movies={upcoming} />
      }
    </section>
  )
}

export default Trending
