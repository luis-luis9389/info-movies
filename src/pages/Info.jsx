import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import Loader from '../components/header/Loader';
import MovieInfo from '../components/infomovie/MovieInfo';
import { API_URL, API_KEY } from '../constants/constants';

const Info = () => {

  let params = useParams();
  const id = params.id;

  const [info, setInfo] = useState();
  const [loading, setLoading] = useState(true);

  const getInfo = async (id) => {
    const res = await fetch(`${API_URL}/movie/${id}?api_key=${API_KEY}&language=es-MX&include_adult=true`);
    const data = await res.json();
    setInfo(data);
    setLoading(false);
  }

  useEffect(() => {
    getInfo(id);
    setLoading(true)
  }, [id]);

  if (loading === true) {
    return (
      <Loader />
    )
  }

  return (
    <section>
      <MovieInfo info={info} />
    </section>
  )
}

export default Info
