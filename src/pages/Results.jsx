import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Loader from '../components/header/Loader';
import MovieList from '../components/movies/MovieList';
import { API_URL, API_KEY } from '../constants/constants';

const Results = () => {

  const params = useParams();
  const q = params.id;

  const [search, setSearch] = useState();
  const [loading, setLoading] = useState(true);

  const searchMovie = async () => {
    const res = await fetch(`${API_URL}/search/movie?api_key=${API_KEY}&language=es-MX&query=${q}&page=1&include_adult=true`);
    const movie = await res.json();
    setSearch(movie);
    setLoading(false);
  }

  useEffect(() => {
    searchMovie();
    setLoading(true)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [q]);

  if (loading === true) {
    return (
      <Loader />
    )
  }

  return (
    <section>
      <MovieList movies={search} />
    </section>
  )
}

export default Results
