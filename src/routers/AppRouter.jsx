import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Header from '../components/header/Header';
import NextPage from '../components/header/NextPage';
import HomePage from '../pages/HomePage';
import Info from '../pages/Info';
import Results from '../pages/Results';
import TopRated from '../pages/TopRated';
import Upcoming from '../pages/Upcoming';

const AppRouter = () => {

  const [page, setPage] = useState(1);

  return (
    <Router>
      <Header page={page} setPage={setPage}/>
      <Switch>
        <Route exact path='/'>
          <HomePage page={page} setPage={setPage}/>
          <NextPage page={page} setPage={setPage} />
        </Route>
        <Route exact path='/top_rated'>
          <TopRated page={page} setPage={setPage}/>
          <NextPage page={page} setPage={setPage} />
        </Route>
        <Route exact path='/upcoming'>
          <Upcoming page={page} setPage={setPage}/>
          <NextPage page={page} setPage={setPage} />
        </Route>
        <Route exact path='/results/:id'>
          <Results />
        </Route>
        <Route exact path='/info/:id'>
          <Info />
        </Route>

        <Redirect to='/' />
      </Switch>
    </Router>
  )
}

export default AppRouter
